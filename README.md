## 立ち上げ方

<pre><code class="bash">bundle exec middleman
</code></pre>

## アクセス

<a href="http://localhost:4567/" target="_blank">http://localhost:4567/</a>


## 編集するSassファイルの場所

<pre><code class="bash">source/stylesheets/application.sass
</code></pre>

と

<pre><code class="bash">source/stylesheets/ディレクトリ/sample
</code></pre>